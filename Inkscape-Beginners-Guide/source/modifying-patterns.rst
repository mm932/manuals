******************
Modifying Patterns
******************

A pattern can easily be rotated, enlarged or shrunk. When the node tool
is active, a gray edit box will appear in the object. Here you can edit the width, heigth, size and rotation of the pattern.
In the Pattern Editor which appears in the Fill and Stroke dialog, gives you even more options for editing the pattern. Here you can also save and name your pattern.


.. figure:: images/pattern_editor1.png
    :alt: Pattern in an ellipse.
    :class: screenshot

    Standard pattern.

.. figure:: images/pattern_editor1.png
    :alt: Enlarged pattern in an ellipse.
    :class: screenshot

    The pattern has been enlarged, by enlarging the edit box.
